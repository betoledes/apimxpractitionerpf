
const mongoose = require('mongoose');
const app = require('./app')
const config = require('./config')

mongoose.connect(config.db, {useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex : true,
  useFindAndModify : false}).then(db => console.log('database is connected!'))
                            .catch(err => console.log(err))

  app.listen(config.port, () => {
    console.log(`todo list RESTful API server started on http://localhost:${config.port}`)
  })
