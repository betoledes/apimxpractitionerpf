const express = require('express');
const userCtrl = require('../controllers/userController')
const userAccess = require('../controllers/userAuth')
const api = express.Router()

//Agregar las funciones de signup y signIn para validar las rutas de acceso
api.get('/users', userCtrl.getUsers)

api.get('/users/:userId', userCtrl.getUser)

api.post('/signUp', userAccess.signUp)

api.post('/login', userAccess.signIn)

api.put('/users/:userId', userCtrl.updateUser)

api.delete('/users/:userId', userCtrl.deleteUser)

api.post('/users/:userId/assignCard', userCtrl.createUserCard)

api.put('/users/:userId/updateCard/:cardId', userCtrl.updateUserCard)

api.delete('/users/:userId/card/:cardId', userCtrl.deleteUserCard)

module.exports = api
