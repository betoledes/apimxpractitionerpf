module.exports = {
  port: process.env.PORT || 3003,
  db: process.env.MONGODB || 'mongodb://localhost/apiPractitioner',
  SECRET_TOKEN: 'mytokenkeypractitioner'
}
