const mongoose = require('mongoose');
const Schema = mongoose.Schema
const bcrypt = require('bcryptjs')

const userSchema = new Schema ({
  email : {type: String, unique: true, lowercase: true},
  username : String,
  password : {type: String},
  signupDate : {type: Date, default: Date.now(), select: false},
  creditCard : [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Card'
  }]
})

userSchema.methods.toJSON = function() {
  let user = this
  let userObject = user.toObject()
  delete userObject.password

  return userObject
}
module.exports = mongoose.model('User', userSchema)
