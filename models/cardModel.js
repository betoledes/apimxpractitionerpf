const mongoose = require('mongoose');
const Schema = mongoose.Schema

const cardSchema = new Schema ({
  cardNumber: String,
  customerName: String,
  customerId: String,
  username: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    select: false },
  cardType: {type: String, enum: ['crédito', 'débito']},
  creditCardCategory: {type: String, enum: ['Azul', 'Oro', 'Platinum', 'Infinite', 'Rayados', 'UNAM', 'No aplica']},
  expirationDate: Date,
  balance: {type: Number, default: 0},
  securityCode: {type: Number, select: false}
})

module.exports = mongoose.model('Card', cardSchema)
