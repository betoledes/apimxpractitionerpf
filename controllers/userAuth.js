const mongoose = require('mongoose');
const User = require('../models/userModel')
const service = require('../services')
const bcrypt = require('bcryptjs')

function signUp(req, res){
  req.body.password = bcrypt.hashSync(req.body.password, 10);
  const user = new User(req.body)
  console.log(user.password)

  user.save((err) => {
    if(err) res.status(500).send({message: `Error to create user: ${err}`})

    return res.status(201).send({token: service.createToken(user)})
  })
}

function signIn(req, res) {
  let body = req.body
  console.log(body)
  console.log('el body',body.email)
  User.findOne({email: body.email}, (err, user) => {
    if(err) return res.status(500).send({message : err})
    if(user != null) {
      console.log('lo que viene de bd',user.email)

      if(!bcrypt.compareSync(body.password, user.password)) return res.status(400).send({message: 'Password incorrect'})

      console.log('lo que viene de bd',user.password)
      console.log('el body',body.password)
      console.log(user)
      res.status(200).send({message : 'Loggin successful', token: service.createToken(user), user})
    } else {
      return res.status(404).send({message : 'User does not exist'})
    }
  })
}

module.exports = {
  signUp,
  signIn
}
