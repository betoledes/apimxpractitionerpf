const User = require('../models/userModel')
const Card = require('../models/cardModel')
const bcrypt = require('bcryptjs');

function getUser (req, res) {
  let userId = req.params.userId
  User.findById(userId, (err, user) =>{
    if(err) return res.status(500).send({message : `Error to get user ${err}`})
    if(!user) return res.status(404).send({message : `User does not exist`});

    User.populate(user, {path: "creditCard"}, (err, user) => {
      res.status(200).send(user)
    })
  })
}

function getUsers (req, res) {
  User.find({}, (err, users) => {
    if(err) return res.status(500).send({message : `Error to get users ${err}`})
    if(!users) return res.status(404).send({message : `Users do not exist`});

    User.populate(users, {path: "creditCard"}, (err, users) => {
      res.status(200).send({users})
    })
  })
}

function updateUser (req, res) {
  let userId = req.params.userId

  let bodyUpdate = req.body

  User.findByIdAndUpdate(userId, bodyUpdate, (err, userUpdated) => {
    if(err) return res.status(500).send({message : `Error to update user ${err}`})
    res.status(200).send(userUpdated)
  })
}

function deleteUser (req, res) {
  let userId = req.params.userId

  User.findById(userId, (err, user) => {
    if(err) return res.status(500).send({message : `Error to delete user ${err}`})

    user.remove(err => {
      if(err) return res.status(500).send({message : `Error to delete user ${err}`})
      res.status(200).send({message : 'User deleted'})
    })
  })
}

function createUserCard (req, res) {
  console.log('/POST/api/users/createCard')
  console.log(req.body)

  let userId = req.params.userId
  let card = new Card()

  card.cardNumber = req.body.cardNumber
  card.customerName = req.body.customerName
  card.customerId = req.body.customerId
  card.username = userId
  card.cardType = req.body.cardType.toLowerCase()
  card.creditCardCategory = req.body.creditCardCategory
  card.expirationDate = req.body.expirationDate
  card.balance = req.body.balance
  card.securityCode = req.body.securityCode

  card.save((err, cardCreated) => {
    if(err) res.status(500).send({message: `Error to save card in database ${err}`})
    console.log(cardCreated)

    User.findByIdAndUpdate(userId, { $push: {creditCard : cardCreated}}, (err, user) => {
      if(err) return res.status(500).send({message : `Error to update user ${err}`})

      res.status(201).send({cardCreated, message : 'Card Created', user: user})
    })
  })
}

function updateUserCard (req, res) {
  console.log('/POST/api/users/updateCard')
  console.log(req.body)
  let userId = req.params.userId
  let cardId = req.params.cardId
  let cardUpdate = req.body
  User.findById(userId, (err, user) => {
    if(err) return res.status(500).send({message : `Error to find user ${err}`})

    Card.findByIdAndUpdate(cardId, cardUpdate, (err, cardUpdated) => {
      if(err) return res.status(500).send({message : `Error to update card ${err}`})
      res.status(200).send(cardUpdated)
    })
  })

}

function deleteUserCard (req, res) {
  let userId = req.params.userId
  let cardId = req.params.cardId
  User.findById(userId, (err, user) => {
    if(err) return res.status(500).send({message : `Error to find user ${err}`})

    Card.findById(cardId, (err, card) => {
      if(err) return res.status(500).send({message : `Error to find card ${err}`})

      card.remove(err => {
        if(err) return res.status(500).send({message : `Error to delete card ${err}`})
        res.status(200).send({message : 'Card deleted'})
      })
    })
  })
}

module.exports = {
  getUser,
  getUsers,
  updateUser,
  deleteUser,
  createUserCard,
  updateUserCard,
  deleteUserCard
}
